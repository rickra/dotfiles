export EDITOR=nvim
export BROWSER=firefox
export TERMINAL=foot
export VISUAL="$EDITOR"
export WORDCHARS='*?_-.[]~=&;!#$%^(){}<>'

# wayland all the things
export MOZ_ENABLE_WAYLAND=1
export MOZ_WEBRENDER=1
export BEMENU_BACKEND=wayland
export BEMENU_OPTS="--no-overlap --nb #3f3f3f --nf #dcdccc --ignorecase --accept-single"
export QT_QPA_PLATFORM=wayland
export CLUTTER_BACKEND=wayland
export SDL_VIDEODRIVER=wayland

# dark mode https://wiki.archlinux.org/title/Dark_mode_switching
export GTK_THEME=Adwaita:dark
export GTK2_RC_FILES=/usr/share/themes/Adwaita-dark/gtk-2.0/gtkrc
export QT_STYLE_OVERRIDE=Adwaita-Dark

export _JAVA_AWT_WM_NONREPARENTING=1

# move xdg dirs to sane places
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.local/cache"
export XDG_DATA_HOME="$HOME/.local/share"

export PATH="$HOME/.local/bin:~/.cargo/bin/:$PATH"

type ruby 1>/dev/null && {
	export GEM_HOME="$(ruby -e 'puts Gem.user_dir')"
	export PATH="$PATH:$GEM_HOME/bin"
}

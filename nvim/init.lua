vim.opt.runtimepath:append('/usr/share/vim/vimfiles')
vim.opt.scrolloff = 10
vim.opt.ignorecase = true
vim.opt.mouse = {}
vim.opt.formatoptions:remove('co')
vim.opt.splitright = true
vim.opt.termguicolors = true

-- disable netrw for nvim-tree
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

--- highlighting settings, see https://github.com/tpope/vim-markdown
vim.g.markdown_minlines = 100
vim.g.markdown_fenced_languages = {'html', 'python', 'vim', 'bash',}
vim.g.tex_flavor = "latex"

--- settings for typst https://github.com/kaarmu/typst.vim#options
vim.g.typst_embedded_languages = vim.g.markdown_fenced_languages

--- command/file completion
vim.opt.wildignorecase = true
vim.opt.wildmode = "list:full"
vim.opt.path:append('**')
--- in-buffer completion
vim.opt.completeopt = 'menuone,noselect'

--- mark files as helm files
vim.api.nvim_create_autocmd({'BufRead', 'BufNewFile', }, {
	pattern = {"*.tpl", },
	command = "set ft=helm",
})

--- enable highlighting only while not in insert
vim.api.nvim_create_autocmd({'InsertEnter', }, {
	pattern = {"*", },
	command = "set nohlsearch",
})
vim.api.nvim_create_autocmd({'CmdlineEnter', }, {
	pattern = {"/,\\?", },
	command = "set hlsearch",
})

--- highlight EOL spaces
vim.opt.list = true
vim.cmd('match errorMsg /\\s\\+$/')
vim.opt.listchars = {tab = '>~', trail = "_"}

--- go to insert mode in terminal immediately
vim.api.nvim_create_autocmd({'TermOpen',}, {
	pattern = {'*',},
	command = 'startinsert',
})

--- ansible detection pattern
vim.api.nvim_create_autocmd({'BufRead', 'BufNewFile'}, {
	pattern = {'*/playbooks/*.yml', },
	command = "set filetype=yaml.ansible",
})

--- .tfvars gets detected as terraform and not as terraform-vars by default
vim.api.nvim_create_autocmd({'BufRead', 'BufNewFile'}, {
	pattern = {'*.tfvars', },
	command = "set filetype=terraform-vars",
})

-- bootstrap lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({"git", "clone", "--filter=blob:none", "https://github.com/folke/lazy.nvim.git", "--branch=stable", lazypath})
end
vim.opt.rtp:prepend(lazypath)

vim.g.mapleader = " "
vim.g.maplocalleader = "\\"

vim.opt.showmode = false  -- mode is shown in lualine

require("lazy").setup({
	"https://tpope.io/vim/fugitive.git",
	{
		"folke/tokyonight.nvim",
		opts={
			style = "moon",
			transparent = true,
		}
	},
	{
		'nvim-lualine/lualine.nvim',
		opts = {},
		config = function(_, opts)
			require('lualine').setup(opts)
		end,
	},
	{
		"nvim-tree/nvim-tree.lua",
		dependencies = { "nvim-tree/nvim-web-devicons", },
		opts = {},
		config = function(_, opts)
			require("nvim-tree").setup(otps)
		end,
	},
	"godlygeek/tabular",
	"chrisbra/Colorizer",

	--- language specific stuff
	"previm/previm",
	"simrat39/rust-tools.nvim",
	{"kaarmu/typst.vim", ft = 'typst', },  -- typst ft + syntax highlighting
	"towolf/vim-helm", -- add helm ft

	--- Completion
	'b0o/schemastore.nvim',
	{
		'saghen/blink.cmp',
		lazy = true,
		dependencies = { 'rafamadriz/friendly-snippets', },
		version = '*',
		opts = {
			keymap = {
				['<Tab>'] = { 'select_next', 'fallback', },
				['<S-Tab>'] = { 'select_prev', 'fallback', },
				['<C-d>'] = { 'scroll_documentation_up', 'fallback', },
				['<C-f>'] = { 'scroll_documentation_down', 'fallback', },
				['<C-Space>'] = { 'show', 'fallback', },
				['<C-e>'] = { 'cancel', 'fallback', },
				['<CR>'] = { 'accept', 'fallback', },
			},
			sources = { default = { 'lsp', 'path', 'snippets', 'buffer', }, },
			fuzzy = { prebuilt_binaries = { force_version='v0.12.4', }, },
			completion = {
				list = { selection = { preselect = false, }, },
				menu = { max_height = 32, },
				documentation = {
					auto_show = true,
				},
				ghost_text = { enabled = true, },
			},
		},
	},

	{
		"neovim/nvim-lspconfig",
		dependencies = {
			'saghen/blink.cmp',
			'b0o/schemastore.nvim',
		},
		opts = {
			servers = {

				ansiblels = {},
				bashls = {},
				ccls = {},
				helm_ls = {},
				jsonls = function() return {
					schemas = require('schemastore').json.schemas(),
					validate = { enable = true },
				} end,

				ltex = {},
				pyright = {},
				rust_analyzer = {},
				terraformls = {},

				texlab = {
					build = {
						-- tectonic does sadly not properly support minted
						-- executable = "tectonic",
						-- args = {"--synctex", "--keep-logs", "--keep-intermediates", "-Z", "shell-escape", "%f",},
						forwardSearchAfter = true,
						onSave = true,
					},
					forwardSearch = {
						executable = "zathura",
						args = {"--synctex-forward", "%l:1:%f", "%p",},
					},
					chktex = {
						onEdit = false,
						onOpenAndSave = true,
					},
				},

				tinymist = {},

				yamlls = function() return {
					schemaStore = {
						enable = false,  -- do management via schemastore.nvim plugin
						url = "",  -- avoid type error
					},
					schemas = require('schemastore').yaml.schemas(),
				} end,

				ltex = {
					-- words that are spelled correctly
					dictionary = {
						["en-US"] = {"ssh",},
						["de-DE"] = {"ssh",},
					},
					markdown = {
						nodes = {
							CodeBlock = "ignore",
							FencedCodeBlock = "ignore",
							AutoLink = "dummy",
							Link = "dummy",
							LinkRef = "ignore",
							Code = "dummy"
						},
					},
					completionEnabled = true,
				},

				tinymist = {
					formatterMode = "typstyle",
					exportPdf = "onSave",
					semanticTokens = "disable"
				},

			},
		},

		config = function(_, opts)
			local lspconfig = require('lspconfig')
			for server, config in pairs(opts.servers) do
				-- if a function, call it. required for using lua dependencies (e.g. schemastore)
				config = (type(config) == "function" and config() or config)
				-- passing config.capabilities to blink.cmp merges with the capabilities in your
				-- `opts[server].capabilities, if you've defined it
				config.capabilities = require('blink.cmp').get_lsp_capabilities(config.capabilities)
				lspconfig[server].setup(config)
			end
		end
	},
})

--- plugins settings
vim.cmd[[colorscheme tokyonight]]

vim.g.previm_open_cmd = "$BROWSER"

--- lsp bindings
vim.keymap.set('n', '<Space>rn', '<Cmd>lua vim.lsp.buf.rename()<CR>')
vim.keymap.set('n', '<Space>x', '<Cmd>lua vim.lsp.buf.references()<CR>')
vim.keymap.set('n', '<Space>s', '<Cmd>lua vim.lsp.buf.workspace_symbol()<CR>')
vim.keymap.set('n', '<Space>t', '<Cmd>lua vim.lsp.buf.type_definition()<CR>')
vim.keymap.set('n', '<Space>i', '<Cmd>lua vim.lsp.buf.implementation()<CR>')
vim.keymap.set('n', '<Space>d', '<Cmd>lua vim.lsp.buf.definition()<CR>')
vim.keymap.set('n', '<Space>c', '<Cmd>lua vim.lsp.buf.incoming_calls()<CR>')
vim.keymap.set('n', '<Space>C', '<Cmd>lua vim.lsp.buf.outgoing_calls()<CR>')
vim.keymap.set('n', '<Space>h', '<Cmd>lua vim.lsp.buf.document_highlight()<CR>')
vim.keymap.set('n', '<Space>H', '<Cmd>lua vim.lsp.buf.clear_references()<CR>')
vim.keymap.set('n', '<Space><Space>', '<Cmd>lua vim.lsp.buf.hover()<CR>')
vim.keymap.set('n', '<Space>a', '<Cmd>lua vim.lsp.buf.code_action()<CR>')
vim.keymap.set('n', '<Space>f', '<Cmd>lua vim.lsp.buf.format()<CR>')
vim.keymap.set('n', '<Space>?', '<Cmd>lua vim.diagnostic.open_float()<CR>')
vim.keymap.set('n', '<Space>/', '<Cmd>lua vim.lsp.buf.workspace_symbol()<CR>')

--- other helpful bindings
vim.keymap.set('n', '<F1>', '<Cmd>w<CR><Cmd>tabe term://%:p<CR>')
vim.keymap.set('n', '<F2>', '<Cmd>tabe +term<CR>')
vim.keymap.set('n', '<F3>', '<cmd>NvimTreeFocus<CR>')  -- OR NvimTreeFindFileToggle

vim.cmd('digraph =v 8659') -- '⇓'
vim.cmd('digraph o+ 8853') -- '⊕'

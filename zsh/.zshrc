source '/usr/share/zsh-antidote/antidote.zsh'
antidote load

[[ -f ~/.config/zsh/local.sh ]] && source ~/.config/zsh/local.sh

export KUBECONFIG=~/.config/kube/config.yaml
export KUBECOLOR_THEME_DEFAULT=none
export KUBECOLOR_OBJ_FRESH="16h"
export LESSHISTFILE="$XDG_CACHE_HOME/lesshst"

alias k='kubecolor'
alias kctx="kubectl config use-context"
alias pls='sudo'
alias fuck='sudo $(fc -ln -1)'
alias ls='ls --color=auto'
alias ll='ls --color=auto -alhp'
alias ip='ip -color=auto'
alias grep='grep --color=auto'
alias diff='diff --color=auto --unified'

alias ga='git add'
alias gd='git diff'
alias gb='git branch'
alias gl='git pull'
alias gp='git push'
alias gc='git commit'
alias gc!='git commit --amend'
alias gcl='git clone'
alias gca='git commit --all'
alias gco='git checkout'
alias gca!='git commit --all --amend'
alias gcam='git commit --all --message'
alias gst='git status'
alias gss='git status --short'
alias glog="git log --format=format:'%C(dim white)%h%C(auto) %s %C(green)%ar %C(blue)%ae %C(dim red)%G?%C(nodim) %C(white)%ai %C(yellow)%D%+b' --stat"
alias glogg="git log --format=format:'%C(dim white)%h%C(auto) %s %C(green)%ar %C(blue)%ae %C(dim red)%G?%C(nodim) %C(white)%ai %C(yellow)%D%+b' --graph"

export LS_COLORS="$(vivid generate tokyonight-moon)"
# see termcap(5) and terminfo(5)
export LESS_TERMCAP_me=$(tput sgr0)  # reset
export LESS_TERMCAP_mb=$(tput bold; tput setaf 2) # blinking
export LESS_TERMCAP_md=$(tput bold; tput setaf 4) # bold
export LESS_TERMCAP_so=$(tput bold; tput setaf 1; tput setab 0) # standout
export LESS_TERMCAP_se=$(tput rmso; tput sgr0) # standout end
export LESS_TERMCAP_us=$(tput smul; tput bold; tput setaf 7) # underline
export LESS_TERMCAP_ue=$(tput rmul; tput sgr0) # underline end
export LESS_TERMCAP_mr=$(tput rev) # reverse
export LESS_TERMCAP_mh=$(tput dim) # half-bright
export LESS_TERMCAP_ZN=$(tput ssubm)
export LESS_TERMCAP_ZV=$(tput rsubm)
export LESS_TERMCAP_ZO=$(tput ssupm)
export LESS_TERMCAP_ZW=$(tput rsupm)
export GROFF_NO_SGR=1

setopt interactivecomments  # allow usage of comments
setopt chase_links          # expand symbolic links and .. (implicit CHASE_DOTS)
setopt auto_param_slash     # complete directories with a trailing /
setopt auto_list            # automatically list choices on ambiguous completion
setopt list_ambiguous       # automatically highlight first element of completion menu
setopt list_packed          # denser menu but different column width
setopt list_types
setopt complete_in_word     # start completion on both ends
setopt globdots             # complete hidden files
setopt share_history

export KEYTIMEOUT=1 # wait time for multi key bindings
export HISTFILE="$XDG_CACHE_HOME/zsh_history"
export HISTSIZE=9001
export SAVEHIST=$HISTSIZE

autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey -- '^[[A'    up-line-or-beginning-search  # arrowup
bindkey -- '^[[B'    down-line-or-beginning-search  # arrowdown
bindkey -- "^r"      history-incremental-pattern-search-backward
bindkey -- "^a"      beginning-of-line
bindkey -- "^e"      end-of-line
bindkey -- '^[[1;5C' forward-word  # ctrl →
bindkey -- '^[[1;5D' backward-word  # ctrl ←
bindkey -- '^[[Z'    reverse-menu-complete  # shift-tab

# prompt marker, see https://codeberg.org/dnkl/foot#jumping-between-prompts
function mark_prompt { print -Pn "\e]133;A\e\\" }
precmd_functions+=(mark_prompt)

# change cursor shape depending on the mode, viins is 'linked' to main by bindkey -v
function zle-keymap-select {
  if [[ "${KEYMAP}" == "vicmd" ]]; then
    printf "\e[1 q"  # block
  else
    printf "\e[5 q"  # beam
  fi
}
zle -N zle-keymap-select
precmd_functions+=(zle-keymap-select)

zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "$XDG_CACHE_HOME/zsh/zcompcache"

zstyle -e ':completion:*:(ssh|scp|sftp|rsh|rsync):hosts' hosts 'reply=(${=${${(f)"$(cat {/etc/ssh_,~/.ssh/known_}hosts(|2)(N) /dev/null)"}%%[# ]*}//,/ })'

# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _match _correct _approximate _prefix
zstyle ':completion:*' completions 1
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' file-sort modification
zstyle ':completion:*' glob 1
zstyle ':completion:*' ignore-parents pwd ..
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list '+' '+m:{[:lower:][:upper:]}={[:upper:][:lower:]}' '+r:|[._-]=** r:|=**' '+l:|=* r:|=*'
zstyle ':completion:*' max-errors 2
zstyle ':completion:*' menu select=1
zstyle ':completion:*' original true
zstyle ':completion:*' squeeze-slashes true
zstyle ':completion:*' substitute 1
zstyle ':completion:*' verbose true
zstyle :compinstall filename "$HOME/.config/zsh/.zshrc"

autoload -Uz compinit
# End of lines added by compinstall

# prevent zcompdump to be placed in $ZDOTDIR
compinit -d $XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION
compdef kubecolor=kubectl

# derived from https://github.com/JanDeDobbeleer/oh-my-posh/blob/main/themes/catppuccin_mocha.omp.json
eval "$(oh-my-posh init zsh --config "$HOME/.config/zsh/.midnighttokyo.omp.json")"
